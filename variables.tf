variable "vsphere_user" {
	description = "Username to login into vsphere"
	type        = "string"
	default     = "Administrator@vsphere.local"
}

variable "vsphere_cluster" {
        description = "Setting the cluster name"
        type        = "string"
        default     = "CIE-CLUSTER"
}

variable "vsphere_password" {
	description = "Password to login into vsphere"
	type        = "string" 
        default     = "CIE2Toast74!!"
}

variable "vsphere_server" {
        description = "Server to login into vsphere"
        type        = "string"
        default     = "69.195.128.117"
}   

variable "vsphere_datacenter" {
  default = "CIE"
  type    = "string"
}

variable "vsphere_datastore" {
  type             = "string"
  default          = "datastore1"
  description      = "the datastore in the cie"
}

variable "vsphere_resource_pool" {
  type              = "string"
  default          = "CIE-RS-POOL"
}

variable "vsphere_network" {
  default          = "cie_group"
  type             = "string"
}
